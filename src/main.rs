use std::fs::read;

use log::{debug, info, trace};

use nom;
use nom::error::{Error, ErrorKind};
use nom::bits::bits;
use nom::bits::streaming::take;
use nom::bytes::complete::tag;
use nom::combinator::{map, peek};
use nom::multi::{count, length_value};
use nom::number::complete::{u8 as nu8, u16 as nu16, u32 as nu32};
use nom::number::Endianness;
use nom::sequence::tuple;
use nom::{Err as NomErr, IResult};

pub mod types;

use types::{
    Architecture,
    DataMessage,
    DefinitionMessage,
    DeveloperFieldDefinition,
    FieldDefinition,
    FileHeader,
    MessageType,
    Record,
};

const DEFAULT_ENDIANNESS: Endianness = Endianness::Little;

const DOTFIT: &str = ".FIT";

type Input<'a> = &'a [u8];

fn main() {
    debug!("Hello, world!");
    env_logger::init();
    parse();
}

fn parse_file_header(i: Input) -> Result<(Input, FileHeader), ()> {
    let (rest, (header_size, protocol_version, profile_version, data_size, tag, crc)) =
        tuple::<_, _, (), _>((
            nu8,
            nu8,
            nu16(DEFAULT_ENDIANNESS),
            nu32(DEFAULT_ENDIANNESS),
            tag(DOTFIT),
            nu16(DEFAULT_ENDIANNESS),
        ))(i).expect("Failed to parse file header");
    // debug!("{:?}", std::str::from_utf8(tag).unwrap());
    Ok((rest, FileHeader {
        header_size,
        protocol_version,
        profile_version,
        data_size,
        crc,
    }))
}

fn parse_field_definition(i: Input) -> Result<(Input, FieldDefinition), NomErr<Error<Input>>> {
    let (rest, (field_definition_number, size, base_type)):
        (Input, (u8, u8, u8)) = tuple::<_, _, Error<Input>, _>((
        nu8,
        nu8,
        nu8
    ))(i).expect("Failed to parse field definition");
    Ok((rest, FieldDefinition::new(field_definition_number, size, base_type)))
}

fn parse_developer_fields(i: Input) -> Result<(Input, DeveloperFieldDefinition), NomErr<Error<Input>>> {
    let (rest, (field_number, size, developer_data_index)):
        (Input, (u8, u8, u8)) = tuple::<_, _, Error<Input>, _>((
        nu8,
        nu8,
        nu8
    ))(i).expect("Failed to parse field definition");
    Ok((rest, DeveloperFieldDefinition::new(field_number, size, developer_data_index)))
}

fn parse_definition_message(
    i: Input,
    developer_data: bool,
    local_message_type: u8
) -> Result<(Input, DefinitionMessage), ()> {
    // println!("input: {:02x?}", i);
    let (rest, (_reserved, architecture, global_message_number, num_fields)):
        (Input, (u8, Architecture, u16, u8)) =
        tuple::<_, _, (), _>((
            nu8,
            map(nu8, Architecture::from),
            nu16(DEFAULT_ENDIANNESS),
            nu8
        ))(i).expect("Failed to parse definition message");
    trace!(
        "architecture: {:?}, global message number: {:?}, num fields: {:?}",
        architecture, global_message_number, num_fields
    );

    println!("Num fields: {}", num_fields);
    let (rest, field_definitions) = count(
        parse_field_definition,
        num_fields as usize,
    )(rest).expect("Failed to parse field definitions");
    println!("Actual num fields: {}", field_definitions.len());
    trace!("Field definitions ({}) {:?}", field_definitions.len(), field_definitions);

    println!("developer field definitions: {:?}", developer_data);
    let (rest, developer_field_definitions) = if developer_data {
        let (rest, num_fields) = nu8::<_, ()>(i).
            expect("Failed to parse num developer field definitions");
        let (rest, developer_field_definitions) = count(
            parse_developer_fields,
            num_fields as usize,
        )(rest).expect("Failed to parse developer field definitions");
        trace!("Developer field definitions{:?}", developer_field_definitions);
        (rest, developer_field_definitions)
    } else {
        (rest, Vec::new())
    };

    Ok((rest, DefinitionMessage::new(
        local_message_type,
        architecture,
        global_message_number,
        field_definitions,
        developer_field_definitions
    )))
}

fn parse_data_message<'a>(
    i: Input<'a>,
    local_message_type: u8,
    definition_messages: &Vec<DefinitionMessage>
) -> Result<(Input<'a>, DataMessage), ()> {
    let definition_message = definition_messages.iter()
        .filter(|dm| dm.local_message_type == local_message_type)
        .next().expect(&*format!("No definition message for local type {:?} found", local_message_type));
    let result = definition_message.parse_data_message_content(i);
    println!("data message result: {:x?}", result.unwrap().1);
    Err(())
}

fn parse_normal_header_record<'a>(
    i: Input<'a>,
    definition_messages: &Vec<DefinitionMessage>
) -> Result<(Input<'a>, Record), ()> {
    // println!("input: {:02x?}", i);
    let (rest, (_header_type, message_type, bit3, _reserved, local_message_type)):
        (Input, (u8, MessageType, u8, u8, u8)) =
        bits::<_, _, Error<(Input, usize)>, Error<Input>, _>(tuple((
            take(1usize),
            map(take::<_, u8, _, _>(1usize), MessageType::from),
            take(1usize),
            take(1usize),
            take(4usize)
        )))(i).expect("Failed to parse normal record header");
    debug!(
        "message type: {:?}, bit 3: {:?}, local message type: {:?}",
        message_type, bit3, local_message_type
    );

    let (rest, message) = match message_type {
        MessageType::DefinitionMessage => {
            let (rest, definition_message) = parse_definition_message(
                rest,
                bit3 == 1,
                local_message_type
            ).expect("Failed to parse definition message");
            trace!("Definition message: {:?}", definition_message);
            (rest, Record::DefinitionMessage(definition_message))
        },
        MessageType::DataMessage => {
            let (rest, data_message) = parse_data_message(
                rest,
                local_message_type,
                definition_messages,
            ).expect("Failed to parse data message");
            debug!("data message: {:?}", data_message);
            (rest, Record::DataMessage(data_message))
        }
    };
    Ok((rest, Record::from(message)))
}

fn parse_compressed_timestamp_header_record<'a>(
    i: Input<'a>,
    definition_messages: &Vec<DefinitionMessage>
) -> Result<(Input<'a>, Record), ()> {
    let (rest, (_header_type, local_message_type, time_offset)):
        (Input, (u8, u8, u8)) =
        bits::<_, _, Error<(Input, usize)>, Error<Input>, _>(tuple((
            take(1usize),
            take(2usize),
            take(5usize)
        )))(i).expect("Failed to parse compressed timestamp record header");
    trace!("local message type: {:?}, time offset: {:?}", local_message_type, time_offset);
    Err(())
}

fn parse_record<'a>(
    i: Input<'a>,
    definition_messages: &Vec<DefinitionMessage>
) -> Result<(Input<'a>, Record), ()> {
    let (rest, header_type): (Input, u8) =
        peek(bits::<_, _, Error<(Input, usize)>, Error<Input>, _>(
            take(1usize)
        ))(i).expect("Failed to parse record header type");
    println!("header type: {}", header_type);

    let (rest, record) = match header_type {
        0 => parse_normal_header_record(rest, definition_messages),
        1 => parse_compressed_timestamp_header_record(rest, definition_messages),
        _ => unreachable!(),
    }.expect("Failed to parse record header");
    debug!("Parsed record: {:#?}", record);

    Ok((rest, record))
}

fn parse() {
    let filename: String = String::from("data/98RK5415.FIT");
    let bytes = read(&filename).unwrap();
    let (mut rest, file_header) =
        parse_file_header(bytes.as_slice())
            .expect("Failed to parse file header");
    debug!("{:?}", &file_header);
    // After parsing header there should be a file_id definition and data messages
    // (file_id.type = 1) before the activity definition and data messages
    let mut field_definitions = Vec::new();
    loop {
        let (r, record) =
            parse_record(rest, &field_definitions)
                .expect("Failed to parse record header");
        match record {
            Record::DefinitionMessage(msg) => field_definitions.push(msg),
            Record::DataMessage(msg) => info!("Got data message: {:?}", msg),
        }
        //println!("{:?}", r);
        rest = r;
    }
}
