use std::convert::From;

use nom::bytes::complete::take;

#[derive(Debug)]
pub struct FileHeader {
    pub header_size: u8,
    pub protocol_version: u8,
    pub profile_version: u16,
    pub data_size: u32,
    pub crc: u16,
}

#[derive(Debug)]
pub struct FieldDefinition {
    pub field_definition_number: u8,
    pub size: u8,
    pub base_type: u8,
}

impl FieldDefinition {
    pub fn new(field_definition_number: u8, size: u8, base_type: u8) -> Self {
        Self {
            field_definition_number,
            size,
            base_type,
        }
    }
}

#[derive(Debug)]
pub struct DeveloperFieldDefinition {
    field_number: u8,
    size: u8,
    developer_data_index: u8,
}

impl DeveloperFieldDefinition {
    pub fn new(field_number: u8, size: u8, developer_data_index: u8) -> Self {
        Self { field_number, size, developer_data_index }
    }
}

#[derive(Debug)]
pub enum Architecture {
    BigEndian,
    LittleEndian,
}

impl From<u8> for Architecture {
    fn from(val: u8) -> Self {
        match val {
            0 => Architecture::LittleEndian,
            1 => Architecture::BigEndian,
            _ => {
                unreachable!();
            },
        }
    }
}

#[derive(Debug)]
pub struct DefinitionMessage {
    // Header
    pub local_message_type: u8,
    // Record content
    pub architecture: Architecture,
    pub global_message_number: u16,
    pub field_definitions: Vec<FieldDefinition>,
    pub developer_field_definitions: Vec<DeveloperFieldDefinition>,
}

impl DefinitionMessage {
    pub fn new(
        local_message_type: u8,
        architecture: Architecture,
        global_message_number: u16,
        field_definitions: Vec<FieldDefinition>,
        developer_field_definitions: Vec<DeveloperFieldDefinition>) -> Self {
        Self {
            local_message_type,
            architecture,
            global_message_number,
            field_definitions,
            developer_field_definitions,
        }
    }

    pub fn parse_data_message_content<'a>(&'a self, i: &'a [u8]) -> Result<(&'a [u8], Vec<&[u8]>), ()> {
        let mut res = Vec::new();
        let mut rest= i;
        for def in self.field_definitions.iter() {
            let (r, val) = take::<_, _, ()>(def.size)(i)
                .expect("Failed to parse data message content");
            res.push(val);
            rest = r;
        }
        Ok((rest, res))
    }
}

#[derive(Debug)]
pub struct DataMessage {
    pub local_message_type: u8,
    pub timestamp: u32,
}

impl DataMessage {
    pub fn from_normal_header(local_message_type: u8, timestamp: u32) -> Self {
        Self {
            local_message_type,
            timestamp,
        }
    }

    pub fn from_compressed_timestamp_header(local_message_type: u8, time_offset: u8, previous_timestamp: u32) -> Self {
        // From FIT DTP R2.3: 4.1.2.2
        let timestamp: u32 = if (time_offset as u32) < (previous_timestamp & 0x0000001F) {
            previous_timestamp + (time_offset as u32 + 0x20)
        } else {
            previous_timestamp + (time_offset as u32)
        };
        Self {
            local_message_type,
            timestamp,
        }
    }
}

#[derive(Debug)]
pub enum MessageType {
    DefinitionMessage,
    DataMessage,
}

impl From<u8> for MessageType {
    fn from(val: u8) -> MessageType {
        println!("Message type from {}", val);
        match val {
            0 => MessageType::DataMessage,
            1 => MessageType::DefinitionMessage,
            _ => unreachable!(),
        }
    }
}

#[derive(Debug)]
pub enum Record {
    DefinitionMessage(DefinitionMessage),
    DataMessage(DataMessage),
}

impl From<DefinitionMessage> for Record {
    fn from(msg: DefinitionMessage) -> Self {
        Self::DefinitionMessage(msg)
    }
}

impl From<DataMessage> for Record {
    fn from(msg: DataMessage) -> Self {
        Self::DataMessage(msg)
    }
}

#[derive(Debug)]
enum FitType {
    Enum(u8),
    S8(i8),
    U8(u8),
    S16(i16),
    U16(u16),
    S32(i32),
    U32(u32),
    String(String),
    Float32(f32),
    Float64(f64),
    U8Z(u8),
    U16Z(u16),
    U32Z(u32),
    BYTE(u8),
}

#[derive(Debug)]
struct BaseType {
    base_type: u8,
    endian_ability: u8,
    base_type_field: u8,
    fit_type: FitType,
    invalid_value: u64,
    size: u8,
}

/*
const BASE_TYPES: [BaseType; 14] = [
    BaseType{base_type: 0, endian_ability: 0, base_type_field: 0x00, fit_type: FitType::Enum, invalid_value: 0xFF, size: 1},
    BaseType{base_type: 1, endian_ability: 0, base_type_field: 0x01, fit_type: FitType::S8, invalid_value: 0x7F, size: 1},
    BaseType{base_type: 2, endian_ability: 0, base_type_field: 0x02, fit_type: FitType::U8, invalid_value: 0xFF, size: 1},
    BaseType{base_type: 3, endian_ability: 1, base_type_field: 0x83, fit_type: FitType::S16, invalid_value: 0x7FFF, size: 2},
    BaseType{base_type: 4, endian_ability: 1, base_type_field: 0x84, fit_type: FitType::U16, invalid_value: 0xFFFF, size: 2},
    BaseType{base_type: 5, endian_ability: 1, base_type_field: 0x85, fit_type: FitType::S32, invalid_value: 0x7FFFFFFF, size: 4},
    BaseType{base_type: 6, endian_ability: 1, base_type_field: 0x86, fit_type: FitType::U32, invalid_value: 0xFFFFFFFF, size: 4},  // Check length
    BaseType{base_type: 7, endian_ability: 0, base_type_field: 0x07, fit_type: FitType::String, invalid_value: 0x00, size: 1},  // NULL terminated string
    BaseType{base_type: 8, endian_ability: 1, base_type_field: 0x88, fit_type: FitType::Float32, invalid_value: 0xFFFFFFFF, size: 4},
    BaseType{base_type: 9, endian_ability: 1, base_type_field: 0x89, fit_type: FitType::Float64, invalid_value: 0xFFFFFFFFFFFFFFFF, size: 8},  // Check length
    BaseType{base_type: 10, endian_ability: 0, base_type_field: 0x0A, fit_type: FitType::U8Z, invalid_value: 0x00, size: 1},
    BaseType{base_type: 11, endian_ability: 1, base_type_field: 0x8B, fit_type: FitType::U16Z, invalid_value: 0x0000, size: 2},
    BaseType{base_type: 12, endian_ability: 1, base_type_field: 0x8C, fit_type: FitType::U32Z, invalid_value: 0x00000000, size: 4},  // Check length,
    BaseType{base_type: 13, endian_ability: 0, base_type_field: 0x0D, fit_type: FitType::BYTE, invalid_value: 0xFF, size: 1},
];
 */

#[derive(Debug)]
enum FileType {
    Device,
    Settings,
    SportSettings,
    Activity,
    Workout,
    Course,
    Schedule,
    Weight,
    Totals,
    Goals,
    BloodPressure,
    Monitoring,
    ActivitySummary,
    DailyMonitoring,
}
